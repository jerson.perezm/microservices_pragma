package tech.jepezdev.clientmanager.application.image;

import tech.jepezdev.clientmanager.domain.Image;

import java.util.Optional;
import java.util.stream.Stream;

public interface ImagePersistence {

    Image create(Image image);

    Optional<Image> readByClientId(Long clientId);

    Optional<Image> readById(String imageId);

    void deleteByClientId(long clientId);

    Stream<Image> readAll();
}
