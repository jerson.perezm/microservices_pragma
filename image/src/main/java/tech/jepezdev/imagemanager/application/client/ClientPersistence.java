package tech.jepezdev.imagemanager.application.client;

import tech.jepezdev.imagemanager.domain.Client;
import tech.jepezdev.imagemanager.domain.DocType;

import java.util.Optional;
import java.util.stream.Stream;

public interface ClientPersistence {

    Stream<Client> readAllWithFilters(Filter filter);

    Client save(Client newClient);

    Optional<Client> read(Long clientId);

    void delete(Long clientId);

    Optional<Client> findByTypeAndDocument(Integer docType, String doc);

    Stream<DocType> readDocTypes();

    DocType readDocType(Integer docType);
}
