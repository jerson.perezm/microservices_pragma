package tech.jepezdev.clientmanager.infrastructure.persistence.client.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.jepezdev.clientmanager.infrastructure.persistence.client.entity.ClientEntity;
import tech.jepezdev.clientmanager.infrastructure.persistence.client.entity.DocTypeEntity;

import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<ClientEntity, Long> {

    Optional<ClientEntity> findClientEntitiesByDocTypeEntityAndDocument(DocTypeEntity docTypeEntity, String document);

}
