package tech.jepezdev.imagemanager.infrastructure.persistence.client;

import lombok.RequiredArgsConstructor;
import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.webjars.NotFoundException;
import tech.jepezdev.imagemanager.application.client.ClientPersistence;
import tech.jepezdev.imagemanager.application.client.Filter;
import tech.jepezdev.imagemanager.domain.Client;
import tech.jepezdev.imagemanager.domain.DocType;

import java.util.Optional;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class ClientServiceAdapter implements ClientPersistence {

    public static final String INVALID_ACCESS_MESSAGE = "Acceso Ilegal a este método en este contexto";
    public static final String SERVICE_NOT_AVAILABLE_MESSAGE = "Servicio de Cliente No Disponible";
    public static final String CLIENT_NOT_FOUND_MESSAGE = "No se encuentra un Cliente con id=%d";
    private final RestTemplate restTemplate;

    @Override
    public Stream<Client> readAllWithFilters(Filter filter) {
        throw new NotYetImplementedException(INVALID_ACCESS_MESSAGE);
    }

    @Override
    public Client save(Client newClient) {
        throw new NotYetImplementedException(INVALID_ACCESS_MESSAGE);
    }

    @Override
    public Optional<Client> read(Long clientId) {
        try {
            return Optional.ofNullable(restTemplate.getForObject(
                    "http://CLIENT/clientes/{clientId}",
                    Client.class,
                    clientId
            ));
        } catch (HttpClientErrorException error) {
            throw new NotFoundException(String.format(CLIENT_NOT_FOUND_MESSAGE, clientId));
        } catch (Exception error) {
            throw new ResourceAccessException(SERVICE_NOT_AVAILABLE_MESSAGE);
        }
    }

    @Override
    public void delete(Long clientId) {
        throw new NotYetImplementedException(INVALID_ACCESS_MESSAGE);
    }

    @Override
    public Optional<Client> findByTypeAndDocument(Integer docType, String doc) {
        throw new NotYetImplementedException(INVALID_ACCESS_MESSAGE);
    }

    @Override
    public Stream<DocType> readDocTypes() {
        throw new NotYetImplementedException(INVALID_ACCESS_MESSAGE);
    }

    @Override
    public DocType readDocType(Integer docType) {
        throw new NotYetImplementedException(INVALID_ACCESS_MESSAGE);
    }
}
