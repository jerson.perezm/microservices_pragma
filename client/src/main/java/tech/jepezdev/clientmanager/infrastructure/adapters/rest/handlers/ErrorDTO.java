package tech.jepezdev.clientmanager.infrastructure.adapters.rest.handlers;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ErrorDTO {
    private final int status;
    private final String message;
    private String description;
    private List<ErrorField> errors = new ArrayList<>();

    public void addFieldError(String path, String message) {
        errors.add(new ErrorField(path, message));
    }

    @Data
    static class ErrorField {
        private final String field;
        private final String message;
    }

}
