package tech.jepezdev.imagemanager.application.client;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Filter {
    private Integer docType;
    private String document;
    private Integer ageGreaterOrEqualTo;
}
