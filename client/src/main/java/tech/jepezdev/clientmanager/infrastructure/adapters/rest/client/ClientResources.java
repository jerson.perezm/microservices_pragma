package tech.jepezdev.clientmanager.infrastructure.adapters.rest.client;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jepezdev.clientmanager.application.client.ClientService;
import tech.jepezdev.clientmanager.application.client.Filter;
import tech.jepezdev.clientmanager.domain.Client;
import tech.jepezdev.clientmanager.infrastructure.adapters.rest.client.dto.ClientInDTO;
import tech.jepezdev.clientmanager.infrastructure.adapters.rest.client.dto.ClientOutDTO;
import tech.jepezdev.clientmanager.infrastructure.adapters.rest.client.dto.DocTypeDTO;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/clientes")
public class ClientResources {

    private final ClientService clientService;
    private final ClientMapper mapper = new ClientMapper();


    @GetMapping(path = "/")
    public ResponseEntity<List<ClientOutDTO>> readAll(
            @RequestParam(name = "doc-type", required = false) Integer docType,
            @RequestParam(name = "doc", required = false) String doc,
            @RequestParam(name = "age", required = false) Integer greaterEqualsThan
    ) {
        Filter filter = Filter.builder()
                .docType(docType)
                .document(doc)
                .ageGreaterOrEqualTo(greaterEqualsThan)
                .build();
        return ResponseEntity.ok().body(
                clientService
                        .readAll(filter)
                        .map(mapper::mapClientToDto)
                        .collect(Collectors.toList()));
    }


    @PostMapping(path = "/")
    public ResponseEntity<ClientOutDTO> create(@RequestBody @Valid ClientInDTO clientInDto) {
        Client client = clientService.create(mapper.mapDtoToClient(clientInDto));
        URI uri = URI.create(
                ServletUriComponentsBuilder
                        .fromCurrentContextPath()
                        .path(String.format("/clientes/%d", client.getId()))
                        .toUriString()
        );
        return ResponseEntity.created(uri).body(mapper.mapClientToDto(client));
    }


    @GetMapping(path = "/{client-id}")
    public ResponseEntity<ClientOutDTO> readById(@PathVariable("client-id") Long clientId) {

        return ResponseEntity.ok().body(
                mapper.mapClientToDto(clientService.read(clientId))
        );
    }


    @PutMapping(path = "/{client-id}")
    public ResponseEntity<ClientOutDTO> update(@RequestBody @Valid ClientInDTO clientInDto, @PathVariable("client-id") Long clientId) {
        if (clientInDto.getId() == null) {
            clientInDto.setId(clientId);
        } else if (!clientInDto.getId().equals(clientId)) {
            throw new IllegalArgumentException("Error en el id enviado");
        }
        Client client = clientService.update(mapper.mapDtoToClient(clientInDto));
        return ResponseEntity.ok().body(mapper.mapClientToDto(client));
    }


    @DeleteMapping(path = "/{client-id}")
    public ResponseEntity<ClientOutDTO> delete(@PathVariable("client-id") Long clientId) {
        clientService.deleteById(clientId);
        return ResponseEntity.noContent().build();
    }


    @GetMapping(path = "/TiposIdentificacion")
    public ResponseEntity<List<DocTypeDTO>> listarTiposDeIdentificacion() {
        return ResponseEntity.ok().body(
                clientService
                        .readDocTypes()
                        .map(DocTypeDTO::new)
                        .collect(Collectors.toList())
        );
    }

}
