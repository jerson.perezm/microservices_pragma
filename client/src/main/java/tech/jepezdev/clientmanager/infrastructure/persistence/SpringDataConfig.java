package tech.jepezdev.clientmanager.infrastructure.persistence;

import lombok.NoArgsConstructor;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = {"tech.jepezdev.clientmanager.infrastructure.persistence.client.repository"})
@ConfigurationProperties("spring.datasource")
@NoArgsConstructor
@EntityScan(basePackages = "tech.jepezdev.clientmanager.infrastructure.persistence.client.entity")
public class SpringDataConfig {


}