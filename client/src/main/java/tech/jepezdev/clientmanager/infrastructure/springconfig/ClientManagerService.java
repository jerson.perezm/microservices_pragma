package tech.jepezdev.clientmanager.infrastructure.springconfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication(scanBasePackages = "tech.jepezdev.clientmanager.infrastructure")
@EntityScan(basePackages = "tech.jepezdev.clientmanager")
public class ClientManagerService {

    public static void main(String[] args) {
        SpringApplication.run(ClientManagerService.class, args);
    }

}
