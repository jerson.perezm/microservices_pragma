package tech.jepezdev.clientmanager.infrastructure.springconfig;



import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import tech.jepezdev.clientmanager.application.client.ClientPersistence;
import tech.jepezdev.clientmanager.application.client.ClientService;
import tech.jepezdev.clientmanager.application.image.ImagePersistence;

@Configuration
public class ClientManagerServiceConfig {


    @Bean
    public ClientService clientService(ClientPersistence clientPersistence, ImagePersistence imagePersistence) {
        return new ClientService(clientPersistence, imagePersistence);
    }


    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}