package tech.jepezdev.clientmanager.infrastructure.adapters.rest.client.dto;

import lombok.Data;
import org.springframework.beans.BeanUtils;
import tech.jepezdev.clientmanager.domain.DocType;

@Data
public class DocTypeDTO {

    private Integer id;
    private String code;
    private String description;

    public DocTypeDTO(DocType docType){
        BeanUtils.copyProperties(docType, this);
    }

}
