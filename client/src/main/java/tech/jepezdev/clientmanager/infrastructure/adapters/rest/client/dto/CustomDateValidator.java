package tech.jepezdev.clientmanager.infrastructure.adapters.rest.client.dto;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validador de Fecha como String en formato yyyy-MM-dd, y que sea anterior a la fecha actual
 */
public class CustomDateValidator implements ConstraintValidator<CustomDateValidatorAnnotation, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        Pattern pattern = Pattern.compile("\\d{4}-\\d{2}-\\d{2}");
        try {
            Matcher matcher = pattern.matcher(value);
            if (!matcher.matches()) {
                return false;
            } else {
                if (Integer.parseInt(value.substring(5, 7)) > 12) {
                    return false;
                }
                if (Integer.parseInt(value.substring(8, 10)) > 31) {
                    return false;
                }
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                Date date = df.parse(value);
                return date.getTime() < new Date().getTime();
            }
        } catch (Exception e) {
            return false;
        }
    }
}




