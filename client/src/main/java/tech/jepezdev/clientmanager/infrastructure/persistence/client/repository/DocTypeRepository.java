package tech.jepezdev.clientmanager.infrastructure.persistence.client.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.jepezdev.clientmanager.infrastructure.persistence.client.entity.DocTypeEntity;

@Repository
public interface DocTypeRepository extends JpaRepository<DocTypeEntity, Integer> {
    // vacía a petición del framework
}
