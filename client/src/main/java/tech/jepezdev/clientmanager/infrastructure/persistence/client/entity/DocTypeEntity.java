package tech.jepezdev.clientmanager.infrastructure.persistence.client.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import tech.jepezdev.clientmanager.domain.DocType;

import javax.persistence.*;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name="document_types")
public class DocTypeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 3, nullable = false)
    private String code;

    @Column(length = 64, nullable = false)
    private String description;

    public DocTypeEntity(Integer id){
        this.id = id;
    }

    public DocType toDocType(){
        DocType docType = new DocType();
        BeanUtils.copyProperties(this, docType);
        return docType;
    }

}
