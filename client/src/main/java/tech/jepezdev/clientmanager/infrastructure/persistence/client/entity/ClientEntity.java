package tech.jepezdev.clientmanager.infrastructure.persistence.client.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import tech.jepezdev.clientmanager.domain.Client;

import javax.persistence.*;
import java.time.LocalDate;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name="clients",
        uniqueConstraints = @UniqueConstraint(
                columnNames = {"doc_type_id", "document"}
        )
)
public class ClientEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(length = 64, nullable = false)
    private String firstName;


    @Column(length = 64, nullable = false)
    private String lastName;


    @ManyToOne(optional = false, cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "doc_type_id", nullable = false)
    private DocTypeEntity docTypeEntity;


    @Column(nullable = false)
    private String document;


    @Column(columnDefinition = "DATE")
    private LocalDate birthDate;


    @Column(length = 128)
    private String birthCity;


    public ClientEntity(Client client) {
        BeanUtils.copyProperties(client, this);
        this.docTypeEntity = new DocTypeEntity(client.getDocType());
    }


    public Client toClient() {
        Client client = new Client();
        BeanUtils.copyProperties(this, client);
        client.setDocType(this.getDocTypeEntity().getId());
        return  client;
    }
}
