package tech.jepezdev.imagemanager.infrastructure.persistence.image.entity;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.bson.types.Binary;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
@Getter
@Setter
@RequiredArgsConstructor
public class ImageEntity {

    @Id
    private String id;

    private Long client;

    private Binary fileContent;
}
