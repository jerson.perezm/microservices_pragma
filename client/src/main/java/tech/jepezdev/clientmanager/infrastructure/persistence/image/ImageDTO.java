package tech.jepezdev.clientmanager.infrastructure.persistence.image;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.net.URI;

@Getter
@Setter
@NoArgsConstructor
public class ImageDTO {

    private Long client;
    private URI url;

}
