package tech.jepezdev.imagemanager.infrastructure.springconfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication(scanBasePackages = "tech.jepezdev.imagemanager.infrastructure")
@EntityScan(basePackages = "tech.jepezdev.imagemanager")
@EnableEurekaClient
public class ImageManagerService {

    public static void main(String[] args) {
        SpringApplication.run(ImageManagerService.class, args);
    }

}
