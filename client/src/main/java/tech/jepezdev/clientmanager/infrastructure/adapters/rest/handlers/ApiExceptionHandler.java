package tech.jepezdev.clientmanager.infrastructure.adapters.rest.handlers;

import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.webjars.NotFoundException;

import java.util.HashMap;
import java.util.List;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@ControllerAdvice
public class ApiExceptionHandler {


    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    protected ResponseEntity<Object> handleValidationException(Exception exception) {
        HttpStatus status = BAD_REQUEST;
        return ResponseEntity.status(status).body(
                buildErrorResponse(
                        status,
                        "Error de validación de datos",
                        ((MethodArgumentNotValidException) exception).getBindingResult().getFieldErrors(),
                        ""
                )
        );
    }

    @ExceptionHandler(value = {DuplicateKeyException.class, NotFoundException.class, Exception.class})
    protected ResponseEntity<Object> handleConflict(Exception exception) {
        HttpStatus status = exception.getClass() == NotFoundException.class ? NOT_FOUND : BAD_REQUEST;
        exception.printStackTrace();
        return ResponseEntity.status(status).body(
                buildErrorResponse(
                        status,
                        exception.getClass().getSimpleName(),
                        null,
                        exception.getLocalizedMessage()
                ));
    }

    private HashMap<String, Object> buildErrorResponse(HttpStatus status, String message, List<FieldError> fieldErrors, String description) {
        ErrorDTO errorDTO = new ErrorDTO(status.value(), message);

        if (fieldErrors != null) {
            for (FieldError fieldError : fieldErrors) {
                errorDTO.addFieldError(fieldError.getField(), fieldError.getDefaultMessage());
            }
        }

        errorDTO.setDescription(description);

        HashMap<String, Object> error = new HashMap<>(1);
        error.put("error", errorDTO);
        return error;
    }


}
