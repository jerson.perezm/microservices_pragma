package tech.jepezdev.clientmanager.infrastructure.persistence.image;

import lombok.RequiredArgsConstructor;
import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import tech.jepezdev.clientmanager.application.image.ImagePersistence;
import tech.jepezdev.clientmanager.domain.Image;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class ImageServiceAdapter implements ImagePersistence {

    public static final String SERVICE_NOT_AVAILABLE_MESSAGE = "Servicio de Imagen No Disponible";
    public static final String IMAGE_SERVICE_BY_CLIENT_ID = "http://IMAGE/clientes/{clientId}/imagen";
    public static final String IMAGE_SERVICE = "http://IMAGE/imagenes/";
    private final RestTemplate restTemplate;


    @Override
    public Image create(Image image) {
        throw new NotYetImplementedException();
    }

    @Override
    public Optional<Image> readByClientId(Long clientId) {
        try {
            return Optional.ofNullable(restTemplate.getForObject(
                    IMAGE_SERVICE_BY_CLIENT_ID,
                    Image.class,
                    clientId
            ));
        } catch (HttpClientErrorException error) {
            return Optional.empty();
        } catch (Exception  error) {
            error.printStackTrace();
            throw new ResourceAccessException(SERVICE_NOT_AVAILABLE_MESSAGE);
        }
    }

    @Override
    public Optional<Image> readById(String imageId) {
        throw new NotYetImplementedException();
    }

    @Override
    public void deleteByClientId(long clientId) {
        try {
            restTemplate.delete(
                    IMAGE_SERVICE_BY_CLIENT_ID,
                    clientId
            );
        } catch (Exception  error) {
            throw new ResourceAccessException(SERVICE_NOT_AVAILABLE_MESSAGE);
        }
    }

    @Override
    public Stream<Image> readAll() {
        try {
            Image[] image = restTemplate.getForObject(
                    IMAGE_SERVICE,
                    Image[].class
            );
            if (image == null) {
                return Stream.empty();
            }
            return Arrays.stream(image);

        } catch (Exception error) {
            throw new ResourceAccessException(SERVICE_NOT_AVAILABLE_MESSAGE);
        }
    }
}
