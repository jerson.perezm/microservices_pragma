package tech.jepezdev.imagemanager.infrastructure.adapters.rest.image;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.bson.internal.Base64;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jepezdev.imagemanager.application.image.ImageService;
import tech.jepezdev.imagemanager.domain.Image;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequiredArgsConstructor
@RequestMapping
public class ImageResources {

    private final ImageService imageService;


    @Operation(summary = "Lista las imágenes registradas como DTO")
    @GetMapping(path = "/imagenes/")
    @ResponseBody
    public ResponseEntity<List<ImageDTO>> readAll() {
        Stream<Image> imagen = imageService.readAll();
        return ResponseEntity.ok().body(imagen.map(ImageDTO::new).collect(Collectors.toList()));
    }


    @Operation(summary = "Devuelve la imagen en formato base 64")
    @ApiResponse(description = "imagen en base 64")
    @GetMapping(path = "/imagenes/{imagen_id}")
    @ResponseBody
    public ResponseEntity<Object> readById(@PathVariable("imagen_id") String imageId) {
        Image imagen = imageService.read(imageId);
        return ResponseEntity.ok()
                //.contentLength(imagen.getFileContent().length())
                .contentType(MediaType.valueOf(MediaType.IMAGE_JPEG_VALUE))
                .body(Base64.decode(imagen.getFileContent()));
    }


    @Operation(summary = "Devuelve el DTO de la imagen relacionada con el client_id")
    @GetMapping(path = "/clientes/{cliente_id}/imagen")
    public ResponseEntity<ImageDTO> readByClientId(@PathVariable("cliente_id") Long clientId) {
        return ResponseEntity.ok().body(new ImageDTO(imageService.readByClientId(clientId)));
    }


    @Operation(summary = "Guarda la imagen relacionada con el client_id")
    @PostMapping(path = "/clientes/{cliente_id}/imagen", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Object> create(@PathVariable("cliente_id") Long clientId, @RequestBody MultipartFile file) throws IOException {
        Image newImage = imageService.create(file, clientId);
        URI uri = URI.create(
                ServletUriComponentsBuilder
                        .fromCurrentContextPath()
                        .path(String.format("/imagenes/%s", newImage.getId()))
                        .toUriString()
        );
        return ResponseEntity.created(uri).build();
    }


    @Operation(summary = "borra la imagen relacionada con el client_id")
    @DeleteMapping(path = "/clientes/{cliente_id}/imagen")
    public ResponseEntity<Object> borrarImage(@PathVariable("cliente_id") Long id) {
        imageService.borrarPorClienteId(id);
        return ResponseEntity.noContent().build();
    }
}
