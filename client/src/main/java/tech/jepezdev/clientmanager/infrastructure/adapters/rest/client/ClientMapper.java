package tech.jepezdev.clientmanager.infrastructure.adapters.rest.client;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import tech.jepezdev.clientmanager.domain.Client;
import tech.jepezdev.clientmanager.infrastructure.adapters.rest.client.dto.ClientInDTO;
import tech.jepezdev.clientmanager.infrastructure.adapters.rest.client.dto.ClientOutDTO;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


/**
 * Define las reglas de mapeo de Cliente a ClienteDTO y viceversa
 */
public class ClientMapper {

    private final ModelMapper modelMapper;
    DateTimeFormatter dateISOFormat = DateTimeFormatter.ISO_LOCAL_DATE;


    public ClientMapper() {

        // crea el mapper
        this.modelMapper = new ModelMapper();

        //define el mapper para pasar Date to String
        TypeMap<Client, ClientOutDTO> stringDateMapper = this.modelMapper.createTypeMap(Client.class, ClientOutDTO.class);
        stringDateMapper.addMappings(
                mapper -> mapper.using(dateStringConverter).map(Client::getBirthDate, ClientOutDTO::setBirthDate)
        );

        //define el mapper para pasar String to Date
        TypeMap<ClientInDTO, Client> dateStringMapper = this.modelMapper.createTypeMap(ClientInDTO.class, Client.class);
        dateStringMapper.addMappings(
                mapper -> mapper.using(stringLocalDateConverter).map(ClientInDTO::getBirthDate, Client::setBirthDate)
        );
    }


    /**
     * Mapea Clientes enClienteDTO
     *
     * @param client - a ser mapeado
     * @return un nuevo ClienteDTO correspondiente
     */
    public ClientOutDTO mapClientToDto(Client client) {
        this.modelMapper.addConverter(dateStringConverter);
        ClientOutDTO clientInDTO = modelMapper.map(client, ClientOutDTO.class);
        clientInDTO.setAge(client.age());

        return clientInDTO;
    }


    /**
     * Mapea ClienteDTO en Entidades Cliente
     *
     * @param clientInDto dto a ser mapeado
     * @return nueva entidad Cliente
     */
    public Client mapDtoToClient(ClientInDTO clientInDto) {
        this.modelMapper.addConverter(stringLocalDateConverter);
        return modelMapper.map(clientInDto, Client.class);
    }


    /**
     * Convertidor de fecha en formato String a Date
     */
    private final Converter<LocalDate, String> dateStringConverter = c -> this.dateISOFormat.format(c.getSource());


    /**
     * Convertidor de Date a String en formato ISO
     */
    private final Converter<String, LocalDate> stringLocalDateConverter = c -> LocalDate.from(
            dateISOFormat.parse(
                    c.getSource()
            )
    );

}
