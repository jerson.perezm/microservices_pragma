package tech.jepezdev.clientmanager.application.client;

import tech.jepezdev.clientmanager.domain.Client;
import tech.jepezdev.clientmanager.domain.DocType;

import java.util.Optional;
import java.util.stream.Stream;

public interface ClientPersistence {

    Stream<Client> readAllWithFilters(Filter filter);

    Client save(Client newClient);

    Optional<Client> read(Long clientId);

    void delete(Long clientId);

    Optional<Client> findByTypeAndDocument(Integer docType, String doc);

    Stream<DocType> readDocTypes();

    DocType readDocType(Integer docType);
}
