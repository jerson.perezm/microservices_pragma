package tech.jepezdev.imagemanager.infrastructure.persistence.image.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import tech.jepezdev.imagemanager.domain.Image;

import java.util.Optional;

@Repository
public interface ImageRepository extends MongoRepository<Image, String> {

    Optional<Image> findImageByClient(Long id);

    Optional<Image> findImageById(String id);

    void deleteImageByClient(long id);

}
