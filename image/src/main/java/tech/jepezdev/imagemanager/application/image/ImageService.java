package tech.jepezdev.imagemanager.application.image;

import lombok.RequiredArgsConstructor;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.web.multipart.MultipartFile;
import org.webjars.NotFoundException;
import tech.jepezdev.imagemanager.application.client.ClientService;
import tech.jepezdev.imagemanager.domain.Image;

import java.io.IOException;
import java.util.Optional;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class ImageService {

    private final ImagePersistence imagePersistence;
    private final ClientService clientService;

    /**
     * guarda una imagen basado en su contenido e id del cliente relacionado
     *
     * @param file contenido de la imagen
     * @param clientId   clave primaria del cliente relacionado
     * @return URL del recurso creado
     * @throws IOException cuando no es posible obtener los bytes de la imagen
     */
    public Image create(MultipartFile file, Long clientId) throws IOException {

        if (file == null || file.getSize() == 0) {
            throw new IOException("Imagen no valida o no vacía");
        }
        Optional<Image> optionalImage = imagePersistence.readByClientId(clientId);
        Image image = optionalImage.orElse(new Image());

        /* crea la entidad imagen*/
        image.setFileContent( new String(Base64.encodeBase64(file.getBytes())));
        image.setClient(clientId);

        /*lanza error si no existe cliente con el id dado*/
        clientService.read(clientId);

        /* guarda la imagen */
        return imagePersistence.create(image);

    }

    /**
     * devuelve una imagen por el id de su propietario
     *
     * @param id id del Cliente del que se quiere la imagen
     * @return objeto imagen encontrada
     */
    public Image readByClientId(Long id) {
        Optional<Image> image = imagePersistence.readByClientId(id);
        return image.orElseThrow(
                () -> new NotFoundException(String.format("No existe Imagen Registrada para el id=%d.", id))
        );

    }

    /**
     * devuelve una imagen pos su id en mongo
     *
     * @param imagenId id del documento en mongodb
     * @return objeto imagen encontrada
     */
    public Image read(String imagenId) {

        Optional<Image> optionalImage = imagePersistence.readById(imagenId);
        return optionalImage.orElseThrow(() -> new NotFoundException("Imagen No Encontrada"));

    }

    /**
     * Borra la imagen relacionada al id del cliente dado
     *
     * @param clienteId - id del cliente al cual borrar la imagen
     */
    public void borrarPorClienteId(Long clienteId) {
        imagePersistence.deleteByClientId(clienteId);
    }

    public Stream<Image> readAll() {
        return imagePersistence.readAll();
    }
}
