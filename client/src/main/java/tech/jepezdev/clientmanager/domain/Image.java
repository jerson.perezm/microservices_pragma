package tech.jepezdev.clientmanager.domain;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.net.URI;

@Getter
@Setter
@NoArgsConstructor
public class Image {

    private Long client;

    private URI url;
}
