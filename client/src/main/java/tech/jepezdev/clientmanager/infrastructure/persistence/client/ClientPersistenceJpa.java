package tech.jepezdev.clientmanager.infrastructure.persistence.client;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import tech.jepezdev.clientmanager.application.client.ClientPersistence;
import tech.jepezdev.clientmanager.application.client.Filter;
import tech.jepezdev.clientmanager.domain.Client;
import tech.jepezdev.clientmanager.domain.DocType;
import tech.jepezdev.clientmanager.infrastructure.persistence.client.entity.ClientEntity;
import tech.jepezdev.clientmanager.infrastructure.persistence.client.entity.DocTypeEntity;
import tech.jepezdev.clientmanager.infrastructure.persistence.client.repository.ClientRepository;
import tech.jepezdev.clientmanager.infrastructure.persistence.client.repository.DocTypeRepository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class ClientPersistenceJpa implements ClientPersistence {

    private final ClientRepository clientRepository;
    private final DocTypeRepository docTypeRepository;
    private final EntityManager entityManager;
    private CriteriaBuilder criteriaBuilder;


    /**
     * devuelve el listado de todos los Clientes filtrados de acuerdo a clienteCriteriaFilter
     *
     * @param filter, objeto con los parámetros de filtrado enviados desde el cliente
     * @return listado de clientes filtrado
     */
    @Override
    public Stream<Client> readAllWithFilters(Filter filter) {
        criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ClientEntity> criteriaQuery = criteriaBuilder.createQuery(ClientEntity.class);
        Root<ClientEntity> clientRoot = criteriaQuery.from(ClientEntity.class);
        Predicate predicate = getPredicate(filter, clientRoot);
        criteriaQuery.where(predicate);
        TypedQuery<ClientEntity> typedQuery = entityManager.createQuery(criteriaQuery);
        return typedQuery
                .getResultList()
                .stream()
                .map(ClientEntity::toClient);
    }


    @Override
    public Optional<Client> read(Long clientId) {
        Optional<ClientEntity> optionalClient = clientRepository.findById(clientId);
        return optionalClient.map(ClientEntity::toClient);
    }


    @Override
    public Optional<Client> findByTypeAndDocument(Integer docType, String doc) {
        DocTypeEntity docTypeEntity = new DocTypeEntity(docType);
        Optional<ClientEntity> optionalClient = clientRepository.findClientEntitiesByDocTypeEntityAndDocument(docTypeEntity, doc);
        return optionalClient.map(ClientEntity::toClient);
    }


    @Override
    public Client save(Client newClient) {
        return clientRepository
                .save(new ClientEntity(newClient))
                .toClient();

    }


    @Override
    public void delete(Long clientId) {
        clientRepository.deleteById(clientId);
    }


    @Override
    public Stream<DocType> readDocTypes() {
        return docTypeRepository.findAll().stream().map(DocTypeEntity::toDocType);
    }

    @Override
    public DocType readDocType(Integer docType) {
        return docTypeRepository.getById(docType).toDocType();
    }


    /**
     * Obtiene el criterio de búsqueda de acuerdo a los filtros enviados por el usuario
     *
     * @param clienteCriteriaFilter - Objeto con la information de filtrado enviada por el usuario
     * @param clienteRoot           - Base para crear el criterio de búsqueda
     * @return predicado con el criterio de búsqueda obtenido
     */
    private Predicate getPredicate(Filter clienteCriteriaFilter, Root<ClientEntity> clienteRoot) {
        List<Predicate> predicates = new ArrayList<>();

        /* agrega el filtro por tipo de documento, solo si fue solicitado */
        if (Objects.nonNull(clienteCriteriaFilter.getDocType())) {
            predicates.add(criteriaBuilder.equal(clienteRoot.get("docTypeEntity"), clienteCriteriaFilter.getDocType()));
        }

        /* agrega el filtro por documento, solo si fue solicitado */
        if (Objects.nonNull(clienteCriteriaFilter.getDocument())) {
            predicates.add(criteriaBuilder.equal(clienteRoot.get("document"), clienteCriteriaFilter.getDocument()));
        }

        /* agrega el filtro por edad, solo si fue solicitado */
        if (Objects.nonNull(clienteCriteriaFilter.getAgeGreaterOrEqualTo())) {
            /* Obtiene la fecha actual */
            LocalDate searchedDate = LocalDate.now();


            /* le resta la cantidad de años - 1, ya que el criterio es mayor de */
            searchedDate = searchedDate.withYear(searchedDate.getYear() - clienteCriteriaFilter.getAgeGreaterOrEqualTo());

            /* la búsqueda se hace respecto a la fecha obtenida */
            predicates.add(criteriaBuilder.lessThanOrEqualTo(clienteRoot.get("birthDate"), searchedDate));
        }
        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));

    }
}
