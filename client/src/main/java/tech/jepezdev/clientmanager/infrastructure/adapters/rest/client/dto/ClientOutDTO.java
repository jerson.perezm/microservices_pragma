package tech.jepezdev.clientmanager.infrastructure.adapters.rest.client.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class ClientOutDTO implements Serializable {

    private Long id;
    private String firstName;
    private String lastName;
    private Integer docType;
    private String document;
    private String birthDate;
    private String birthCity;
    private String imageUrl;
    private Integer age;
}
