package tech.jepezdev.imagemanager.infrastructure.springconfig;


import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import tech.jepezdev.imagemanager.application.client.ClientPersistence;
import tech.jepezdev.imagemanager.application.client.ClientService;
import tech.jepezdev.imagemanager.application.image.ImagePersistence;
import tech.jepezdev.imagemanager.application.image.ImageService;

@Configuration
public class ImageManagerServiceConfig {

    @Bean
    public ClientService clientService(ClientPersistence clientPersistence, ImagePersistence imagePersistence) {
        return new ClientService(clientPersistence, imagePersistence);
    }

    @Bean
    public ImageService imageService(ImagePersistence imagePersistence, ClientService clientService) {
        return new ImageService(imagePersistence, clientService);
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}