package tech.jepezdev.imagemanager.infrastructure.adapters.rest.image;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jepezdev.imagemanager.domain.Image;

import java.net.URI;

@Getter
@Setter
@NoArgsConstructor
public class ImageDTO {

    public ImageDTO(Image image) {
        this.client = image.getClient();
        this.url = URI.create(
                ServletUriComponentsBuilder
                        .fromCurrentContextPath()
                        .path(String.format("/imagenes/%s", image.getId()))
                        .toUriString()
        );
    }

    private Long client;
    private URI url;

}
