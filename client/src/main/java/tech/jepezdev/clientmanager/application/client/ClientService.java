package tech.jepezdev.clientmanager.application.client;


import lombok.RequiredArgsConstructor;
import org.springframework.dao.DuplicateKeyException;
import org.webjars.NotFoundException;
import tech.jepezdev.clientmanager.application.image.ImagePersistence;
import tech.jepezdev.clientmanager.domain.Client;
import tech.jepezdev.clientmanager.domain.DocType;
import tech.jepezdev.clientmanager.domain.Image;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class ClientService {

    private final ClientPersistence clientPersistence;
    private final ImagePersistence imagePersistence;


    public Stream<Client> readAll(Filter filter) {
        Stream<Client> clientStream = clientPersistence.readAllWithFilters(filter);
        List<Image> imageList = imagePersistence.readAll().collect(Collectors.toList());

        return clientStream.map(client -> {
            Optional<Image> foundImage = imageList
                    .stream()
                    .filter(image -> Objects.equals(image.getClient(), client.getId()))
                    .findFirst();

            foundImage.ifPresent(image -> client.setImageURL(image.getUrl()));
            return client;
        });
    }


    public Client create(Client client) {
        Optional<Client> optionalExistingClient = clientPersistence.findByTypeAndDocument(
                client.getDocType(),
                client.getDocument()
        );

        if (optionalExistingClient.isPresent()) {
            DocType docType = readDocType(optionalExistingClient.get().getDocType());
            throw new DuplicateKeyException(String.format("Ya existe un Cliente con %s = %s",
                    docType.getCode(),
                    optionalExistingClient.get().getDocument()
            ));
        }

        return clientPersistence.save(client);
    }


    public Client read(Long clientId) {
        Optional<Client> optionalClient = clientPersistence.read(clientId);
        optionalClient.ifPresent(client -> imagePersistence
                .readByClientId(clientId)
                .ifPresent(image -> client.setImageURL(image.getUrl()))
        );

        return optionalClient.orElseThrow(() -> new NotFoundException(String.format("No se encuentra un Cliente con id=%d", clientId)));
    }


    public Client update(Client client) {

        // lanza excepción si el cliente no existe
        this.read(client.getId());

        Optional<Client> optionalExistingClient = clientPersistence.findByTypeAndDocument(
                client.getDocType(),
                client.getDocument()
        );

        if (optionalExistingClient.isPresent() && !Objects.equals(optionalExistingClient.get().getId(), client.getId())) {
            DocType docType = readDocType(optionalExistingClient.get().getDocType());
            throw new DuplicateKeyException(String.format("Ya existe un Cliente con %s = %s",
                    docType.getCode(),
                    optionalExistingClient.get().getDocument()
            ));
        }

        Client updatedClient = clientPersistence.save(client);

        imagePersistence
                .readByClientId(updatedClient.getId())
                .ifPresent(image -> updatedClient.setImageURL(image.getUrl()));
        return updatedClient;

    }


    public void deleteById(Long clientId) {
        clientPersistence.delete(clientId);
        imagePersistence.deleteByClientId(clientId);
    }


    public Stream<DocType> readDocTypes() {
        return clientPersistence.readDocTypes();
    }


    public DocType readDocType(int docTypeId) {
        return clientPersistence.readDocType(docTypeId);
    }
}
