package tech.jepezdev.clientmanager.infrastructure.adapters.rest.client.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@NoArgsConstructor
public class ClientInDTO implements Serializable {

    private Long id;

    @NotBlank(message = "Este campo no puede estar vacío")
    @Size(max = 128, message = "Cadena muy Larga")
    private String firstName;

    @NotBlank(message = "Este campo no puede estar vacío")
    @Size(max = 128, message = "Cadena muy Larga")
    private String lastName;

    @Min(value = 1, message = "Tipo de Identificacion No Valido")
    @Max(value = 7, message = "Tipo de Identificacion No Valido")
    private Integer docType;

    @NotBlank(message = "Este campo no puede estar vacío")
    @Size(min = 4, max = 20, message = "documento No válido")
    private String document;

    @NotBlank(message = "Este campo no puede estar vacío")
    @CustomDateValidatorAnnotation(message = "Fecha No Valida, formato Válido: 2000-12-25")
    private String birthDate;

    @NotBlank(message = "Este campo no puede estar vacío")
    @Size(max = 128, message = "Cadena demasiado larga")
    private String birthCity;

}
