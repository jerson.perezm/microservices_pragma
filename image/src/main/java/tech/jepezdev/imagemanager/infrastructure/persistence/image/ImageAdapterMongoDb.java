package tech.jepezdev.imagemanager.infrastructure.persistence.image;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import tech.jepezdev.imagemanager.application.image.ImagePersistence;
import tech.jepezdev.imagemanager.domain.Image;
import tech.jepezdev.imagemanager.infrastructure.persistence.image.repository.ImageRepository;

import java.util.Optional;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class ImageAdapterMongoDb implements ImagePersistence {

    private final ImageRepository imageRepository;


    @Override
    public Optional<Image> readByClientId(Long clientId) {
        return imageRepository.findImageByClient(clientId);
    }


    @Override
    public Optional<Image> readById(String imageId) {
        return imageRepository.findImageById(imageId);
    }


    @Override
    public void deleteByClientId(long clientId) {
        imageRepository.deleteImageByClient(clientId);
    }


    @Override
    public Image create(Image image) {
        return imageRepository.save(image);
    }

    @Override
    public Stream<Image> readAll() {
        return imageRepository.findAll().stream();
    }
}
