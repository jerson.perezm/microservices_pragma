package tech.jepezdev.clientmanager.domain;

import lombok.Data;

@Data
public class DocType {
    private Integer id;
    private String code;
    private String description;
}
