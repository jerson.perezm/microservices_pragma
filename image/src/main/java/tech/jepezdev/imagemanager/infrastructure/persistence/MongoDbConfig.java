package tech.jepezdev.imagemanager.infrastructure.persistence;

import lombok.NoArgsConstructor;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = "tech.jepezdev.imagemanager.infrastructure.persistence.image.repository")
@ConfigurationProperties("spring.data.mongodb")
@NoArgsConstructor
@EntityScan(basePackages = "tech.jepezdev.imagemanager.infrastructure.persistence.image.entity")
public class MongoDbConfig {

}