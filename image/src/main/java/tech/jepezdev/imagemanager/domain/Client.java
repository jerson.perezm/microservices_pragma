package tech.jepezdev.imagemanager.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.Period;

@Data
@NoArgsConstructor
public class Client {

    private Long id;
    private String firstName;
    private String lastName;
    private Integer docType;
    private String document;
    private LocalDate birthDate;
    private String birthCity;
    private String imageId;


    public int age() {
        LocalDate today = LocalDate.now();
        return Period.between(birthDate, today).getYears();
    }
}
