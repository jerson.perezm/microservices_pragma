package tech.jepezdev.imagemanager.domain;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Image {

    private String id;

    private Long client;

    private String fileContent;
}
